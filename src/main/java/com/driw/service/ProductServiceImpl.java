package com.driw.service;

import com.driw.pojo.Box;
import com.driw.pojo.BoxDiscount;
import com.driw.pojo.Product;
import com.driw.repository.BoxDiscountRepository;
import com.driw.repository.BoxRepository;
import com.driw.repository.ProductRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by Sudam on 3/29/2019.
 */
@Service("ProductService")
@Transactional
public class ProductServiceImpl implements ProductService{

    private static final Logger logger = LogManager.getLogger(ProductServiceImpl.class);

    @Autowired
    private BoxRepository boxRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private BoxDiscountRepository boxDiscountRepository;

    private static Double ONE_HUNDRED = 100.00;

    @Override
    public Double calculateTotalPrice(List<Product> productList){
        Double totalCost = 0D;
        for(Product product : productList){
            Double productCost = 0d;
            if(product.getQuantity() != null && product.getQuantity() != 0){
                productCost = calculateProductPrice(product);
            }
            totalCost += productCost;
        }
        return totalCost;

    }

    public Double calculateProductPrice(Product product){
        Double productCost = 0D;
        Map<Box, Integer> boxQuantityMap =  productBoxing(product);
        for(Box box : boxQuantityMap.keySet()){
            if(box != null) {
                Integer numberOfBoxes = boxQuantityMap.get(box);
                Double boxPrice = box.getPrice();

                List<BoxDiscount> boxDiscountList = boxDiscountRepository.findByBoxId(box.getId());
                for(BoxDiscount boxDiscount : boxDiscountList){
                    if(numberOfBoxes >= boxDiscount.getQuantity()){
                        boxPrice -= boxPrice*boxDiscount.getPercentage()/ONE_HUNDRED;
                    }
                }
                productCost += (numberOfBoxes * boxPrice);

            } else {
                List<Box> boxList = boxRepository.findByProductId(product.getId());
                Double itemPrice = (boxList.get(0).getPrice()/boxList.get(0).getQuantity() )* (ONE_HUNDRED + product.getPricePercentage())/ONE_HUNDRED;
                Integer numberOfProducts = boxQuantityMap.get(box);
                productCost += (numberOfProducts * itemPrice);
            }
        }
        logger.info("Product Cost " + product.getCode() + " : "+productCost);
        return productCost;
    }


    public Map<Box, Integer> productBoxing(Product product){

        Map<Box, Integer> boxQuantityMap = new HashMap<>();
        Integer remainingProduct = product.getQuantity();

        List<Box> boxList = boxRepository.findByProductId(product.getId());
        for(Box box : boxList){
            if(product.getQuantity() >= box.getQuantity()) {
                Integer boxes = product.getQuantity() / box.getQuantity();
                boxQuantityMap.put(box, boxes);
                remainingProduct = remainingProduct - boxes * box.getQuantity();
            }
        }
        boxQuantityMap.put(null,remainingProduct);
        return boxQuantityMap;

    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Page<Product> findPaginated(Pageable pageable, Integer productId) {
        Map<Integer, List<Product>> productMap = buildProductMap();
        List<Product> products;
        if(productId == null){
            products = new ArrayList<>();
        } else {
            products = productMap.get(productId);
        }

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Product> list;

        if (products.size() < startItem) {
        list = Collections.emptyList();
        } else {
        int toIndex = Math.min(startItem + pageSize, products.size());
        list = products.subList(startItem, toIndex);
        }

        Page<Product> bookPage = new PageImpl<Product>(list, PageRequest.of(currentPage, pageSize), products.size());
        return bookPage;
    }

    private Map<Integer, List<Product>> buildProductMap()  {
        Map<Integer, List<Product>> productMap = new HashMap<>();
        List<Product> originalProducts = productRepository.findAll();
        try {
            for(Product product : originalProducts) {
                List<Product> productsViewList = new ArrayList<>();
                for (int i = 1; i <= 50; i++) {
                    Product product1 = null;
                    product1 = product.clone();
                    product1.setQuantity(i);
                    product1.setPrice(calculateProductPrice(product1));
                    productsViewList.add(product1);
                }
                productMap.put(product.getId(), productsViewList);
            }
        } catch (CloneNotSupportedException e) {
            logger.error(e.getMessage());
        }
        return productMap;
    }
}

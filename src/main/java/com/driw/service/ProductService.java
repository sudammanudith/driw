package com.driw.service;

import com.driw.pojo.Box;
import com.driw.pojo.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * Created by Sudam on 3/29/2019.
 */
public interface ProductService {

    Double calculateTotalPrice(List<Product> productList);

    Double calculateProductPrice(Product product);

    List<Product> getAllProducts();

    Map<Box, Integer> productBoxing(Product product);

    Page<Product> findPaginated(Pageable pageable, Integer productId);
}

package com.driw.controller;

import com.driw.model.ProductListWrapper;
import com.driw.view.Views;
import com.driw.pojo.Product;
import com.driw.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by Sudam on 3/28/2019.
 */
@Controller
public class ProductListController {

    @Autowired
    private ProductService productService;

    @GetMapping("/calculator")
    public String main(Model model) {

        List<Product> productList = productService.getAllProducts();
        setDefaultData(model, 0D,productList);
        return Views.CALCULATOR.getValue(); //view
    }

    @RequestMapping(value = "/calculator", method= RequestMethod.POST)
    public String calculate(ProductListWrapper productListWrapper, Model model) {
        Double totalCost = productService.calculateTotalPrice(productListWrapper.getList());
        setDefaultData(model,totalCost, productListWrapper.getList());
        return Views.CALCULATOR.getValue();
    }

    public void setDefaultData(Model model, Double totalCost, List<Product> productList){

        ProductListWrapper productListWrapper = new ProductListWrapper();
        productListWrapper.setList(productList);

        model.addAttribute("totalCost", totalCost);
        model.addAttribute("module", "module");
        model.addAttribute("productListWrapper", productListWrapper);
    }

}

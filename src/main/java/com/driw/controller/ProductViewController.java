package com.driw.controller;

import com.driw.view.Views;
import com.driw.pojo.Product;
import com.driw.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class ProductViewController {

    // inject via application.properties
    @Value("${welcome.message}")
    private String message;
    @Autowired
    private ProductService productService;

    /*@GetMapping("/")
    public String main(Model model) {
        model.addAttribute("message", message);
        model.addAttribute("tasks", tasks);

        return "welcome"; //view
    }*/

/*
    // /hello?name=kotlin
    @GetMapping("/hello")
    public String mainWithParam(
            @RequestParam(name = "name", required = false, defaultValue = "") String name, Model model) {

        model.addAttribute("message", name);

        return "welcome"; //view
    }
*/


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String listProducts( Model model,
                                @RequestParam("page") Optional<Integer> page,
                                @RequestParam("size") Optional<Integer> size,
                                @RequestParam("productId") Optional<Integer> productId) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);
        int selectedProductId = productId.orElse(1);

        Page<Product> productPage = productService.findPaginated(PageRequest.of(currentPage - 1, pageSize), selectedProductId);
        List<Product> productList = productService.getAllProducts();

        model.addAttribute("productList", productList);
        model.addAttribute("selectedProductId", selectedProductId);
        model.addAttribute("productPage", productPage);
        model.addAttribute("message", message);

        int totalPages = productPage !=null ? productPage.getTotalPages() : 0;
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
            .boxed()
            .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return Views.PRODUCT_VIEW.getValue();
    }

}
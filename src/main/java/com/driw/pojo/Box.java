package com.driw.pojo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Sudam on 3/28/2019.
 */
@Getter
@Setter
@Entity
@Table(name = "box")
public class Box {

    @Id
    @GeneratedValue
    private int id ;
    @NotNull
    @Column(name = "productid")
    private int productId ;
    @NotNull
    private String code ;
    @NotNull
    private String name ;
    @NotNull
    private Integer quantity ;
    @NotNull
    private Double price ;
}

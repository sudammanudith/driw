package com.driw.pojo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Sudam on 3/29/2019.
 */
@Getter
@Setter
@Entity
@Table(name = "boxdiscount")
public class BoxDiscount {

    @Id
    @GeneratedValue
    private int id ;

    @Column(name = "boxid")
    private Integer boxId;

    private Double percentage;

    private Integer quantity;

}


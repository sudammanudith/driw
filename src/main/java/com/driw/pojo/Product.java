package com.driw.pojo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Sudam on 3/28/2019.
 */
@Getter
@Setter
@Entity
@Table(name = "product")
public class Product implements Cloneable {

    @Id
    @GeneratedValue
    private int id;
    @NotNull
    private String name;
    @NotNull
    private String code;
    @NotNull
    @Column(name = "pricepercentage")
    private Double pricePercentage;
    @Transient
    private Integer quantity;
    @Transient
    private Double price;

    @Override
    public int hashCode(){
        return id;
    }

    @Override
    public Product clone() throws CloneNotSupportedException {
        return (Product)super.clone();
    }

}

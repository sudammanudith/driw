package com.driw.repository;

import com.driw.pojo.Box;
import com.driw.pojo.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Sudam on 3/29/2019.
 */
public interface BoxRepository extends JpaRepository<Box, Integer> {

    @Query("SELECT p FROM Box p WHERE LOWER(p.productId) = LOWER(:productId)")
    List<Box> findByProductId(@Param("productId") Integer productId);
}

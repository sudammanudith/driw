package com.driw.repository;

import com.driw.pojo.Box;
import com.driw.pojo.BoxDiscount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Sudam on 3/29/2019.
 */
public interface BoxDiscountRepository extends JpaRepository<BoxDiscount, Integer> {

    @Query("SELECT bd FROM BoxDiscount bd WHERE LOWER(bd.boxId) = LOWER(:boxId)")
    List<BoxDiscount> findByBoxId(@Param("boxId") Integer boxId);
}

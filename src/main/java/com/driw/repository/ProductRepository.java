package com.driw.repository;

import com.driw.pojo.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Sudam on 3/28/2019.
 */
public interface ProductRepository extends JpaRepository<Product, Integer> {


}

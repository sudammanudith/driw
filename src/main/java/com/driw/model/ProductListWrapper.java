package com.driw.model;

import com.driw.pojo.Product;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by Sudam on 3/29/2019.
 */
@Getter
@Setter
public class ProductListWrapper {
    List<Product> list;
}

package com.driw.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Sudam on 3/29/2019.
 */
@Getter
@Setter
public class SimpleCalculationModel {
    Integer penguinEarCount;
    Integer horseShoesEarCount;
}

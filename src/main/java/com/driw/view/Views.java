package com.driw.view;

public enum Views {
    PRODUCT_VIEW("productView"),
    CALCULATOR("calculator");

    private final String view;

    Views(String view) {
        this.view = view;

    }

    public String getValue() {
        return this.view;
    }
}

package com.driw.controller;

import com.driw.model.ProductListWrapper;
import com.driw.pojo.Product;
import com.driw.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ProductListController.class)
public class ProductControllerTest{

        @Autowired
        private MockMvc mockMvc;
        @MockBean
        private ProductService productService;


        @Test
        public void products() throws Exception {

            ResultActions resultActions = mockMvc.perform(get("/calculator"))
                    .andExpect(status().isOk())
                    .andExpect(view().name("calculator"))
                    .andExpect(model().attribute("totalCost", is(0d)));

        }

        @Test
        public void calculateTotalPrice() throws Exception
        {
            List<Product> productList  = mockProducts();
            ProductListWrapper productListWrapper = new ProductListWrapper();
            productListWrapper.setList(productList);
            mockMvc.perform( MockMvcRequestBuilders
                    .post("/calculator")
                    .content(asJsonString(productListWrapper))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(model().attribute("totalCost", is(0d)));
        }

        public List<Product> mockProducts(){
            List<Product> productList = new ArrayList<Product>();
            Product product1 = new Product();
            product1.setId(0);product1.setQuantity(0);
            productList.add(product1);
            return productList;
        }

        public static String asJsonString(final Object obj) {
            try {
                return new ObjectMapper().writeValueAsString(obj);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }


}

package com.driw.service;

import com.driw.pojo.Box;
import com.driw.pojo.Product;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;

/**
 * Created by Sudam on 4/1/2019.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTest {

    @Autowired
    private ProductService productService;


    @Test
    public void testGetAllProducts() {
        try {
            List<Product> productList = productService.getAllProducts();
            Assert.assertNotNull(productList);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
            throw e;
        }
    }

    @Test
    public void testProductBoxing() {
        try {
            List<Product> productList = productService.getAllProducts();
            Product product = productList.get(0);
            product.setQuantity(25);
            Map<Box, Integer> map = productService.productBoxing(product);
            Assert.assertNotNull(map);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
            throw e;
        }
    }

    @Test
    public void testFindPaginated() {
        try {
            List<Product> productList = productService.getAllProducts();
            Product product = productList.get(0);
            product.setQuantity(25);
            Page<Product> page = productService.findPaginated(PageRequest.of(0, 5), 1);
            Assert.assertNotNull(page);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
            throw e;
        }
    }

    @Test
    public void testCalculateTotalPrice() {
        try {
            List<Product> productList = productService.getAllProducts();
            productList.get(0).setQuantity(25);;
            productList.get(1).setQuantity(8);;
            Double cost = productService.calculateTotalPrice(productList);
            assertThat(cost).isEqualTo(1700.375);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
            throw e;
        }
    }

    @Test
    public void testCalculateProductPrice() {
        try {
            List<Product> productList = productService.getAllProducts();
            productList.get(0).setQuantity(25);;
            Double cost = productService.calculateProductPrice(productList.get(0));
            assertThat(cost).isEqualTo(231.875);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
            throw e;
        }
    }

}
